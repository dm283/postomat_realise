# ПРИЛОЖЕНИЕ ИМИТИРУЮЩЕЕ РАБОТУ ПОСТАМАТА
import socket
import json
import datetime
import sqlite3

def load_settings():
    """загрузка json-настроек при запуске"""
    with open("settings.json", "r") as read_file:
        settings = json.load(read_file)
        
    serial_number = settings["serial_number"]
    ip = settings["keyboard_socket"].partition(':')[0]
    port = int(settings["keyboard_socket"].partition(':')[2])
    
    return serial_number, ip, port

#   = функции для работы с базой данных =
def code_check(buf, cursor):
    """проверка наличия кода в таблице cells"""
    if not buf:
        return 1
    sql = f"SELECT * FROM cells where code = {buf}"
    cursor.execute(sql)
    if cursor.fetchone():
        return 0  # record exists
    else:
        return 1  # error, record not exists

def clear_cell(buf, cursor, conn):
    """удаление записи из таблицы cells"""
    sql = f"DELETE FROM cells WHERE code = {buf}"
    cursor.execute(sql)
    conn.commit()

def calc_nxt_id(cursor):
    """вычисление следующего id"""
    sql = f"select max(id) from events"
    cursor.execute(sql)
    tmp = cursor.fetchone()
    if tmp[0]:
        return tmp[0]+1
    return 1

def write_event(event, cursor, conn):
    """вставка записи в events"""
    eid = calc_nxt_id(cursor)
    time = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    sql = f"insert into events values ('{eid}', '{event}', '{time}')"
    cursor.execute(sql)
    conn.commit()
#   ====

def cmd_handling(command, buf, cursor, conn):
    """обработчик команд"""
    c = command[(command.index(':')+1):]              # получили кнопку из команды
    #switch(c)
    if c == "OK":
        if code_check(buf, cursor):                   # проверяем код в базе cells (0 = есть 1 = нет)
            write_event("CODE_ERROR", cursor, conn)
        else:
            write_event("CODE_ENTERED", cursor, conn)
            write_event("DELIVERED", cursor, conn)
            clear_cell(buf, cursor, conn)             # удаляем в базе cells = запись с этой посылкой
        buf = ""
    elif c == "DEL":
        # удалили последнюю введенныу цифру
        buf = buf[:-1]
        buf
    elif c in "0123456789":
        # записали в буфер цифру
        if len(buf) < 9:
            buf += c  
    return buf


def create_soc(ip, port):
    sock = socket.socket()
    sock.bind((ip, port))
    sock.listen(1)
    conn_soc, addr = sock.accept()    
    return (conn_soc, addr)


def create_db_connect():
    conn_db = sqlite3.connect("shiptor.db")
    cursor = conn_db.cursor()
    return (conn_db, cursor)


def main():
    serial_number, ip, port = load_settings()  #загрузка настроек
    #print(serial_number, ip, port)  ##
    
    conn_soc, addr = create_soc(ip, port)      #создание сокета
    #print ('connected:', addr)  ##
    
    conn_db, cursor = create_db_connect()      #подключение к базе данных
    
    buf = str()                                #инициализация буфера
    
    #блок обработки событий
    while True:
        data = conn_soc.recv(1024)             #пришла команда с клавиатуры
        if not data:
            break
        command = data.decode("utf-8")
        #print(command) ##
        
        buf = cmd_handling(command, buf, cursor, conn_db)
        #print(buf)  ##
        
    conn_soc.close()                           #закрытие сокета
    
    return 0


if __name__ == '__main__':
    main()
