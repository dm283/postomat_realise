# СИМУЛЯТОР РАББОТЫ КЛАВИАТУРЫ (ДЛЯ ОТЛАДКИ)
import socket

ip = "127.0.0.1"
port = 5001

sock = socket.socket()
sock.connect((ip, port))

while True:
    k = str(input())
    if k == 'q':
        sock.close()
        break
    sock.send( ("KBD.PRESS:"+k).encode("utf-8") )

sock.close()
